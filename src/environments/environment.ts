// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBe5M-Ye3t3NjJZpwbYNvIMjrTw1rT4DKQ',
    authDomain: 'stage-95b19.firebaseapp.com',
    projectId: 'stage-95b19',
    storageBucket: 'stage-95b19.appspot.com',
    messagingSenderId: '59443388824',
    appId: '1:59443388824:web:e949aa09303cc856ef0bed'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
