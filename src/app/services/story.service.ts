import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { combineLatest, concat, Observable, of } from 'rxjs';
import { Story } from '../models/story';
import { catchError, filter, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { AngularFirestore, DocumentChangeAction, DocumentReference, QueryDocumentSnapshot } from '@angular/fire/firestore';
import { ResponseService } from './response.service';
import { MessagesService } from './messages.service';


@Injectable({
  providedIn: 'root'
})
export class StoryService {

  stories$: Observable<Story[]>;

  constructor(private db: AngularFirestore, private responseService: ResponseService, private messagesService: MessagesService) {
    this.stories$ = this.getStories();
   }

  /** GET stories from the server */
  /*getStories(): Observable<Story> {
    return this.db.collection('stories').snapshotChanges()
    .pipe(
      tap(x => console.log('service', x)),
      map( stories => stories
      .map(story => {
        const id = story.payload.doc.id;
        const doc = story.payload.doc;
        return {id, doc};
      })),
      switchMap((stories: any) => of({} as Story)),
      tap(x => console.log('service 2', x.doc.collection('responses'))),
      );
  }*/

  getStories(): Observable<Story[]> { // fetch les stories sans leur reponse, juste info hasResponse
    return this.db.collection('stories').valueChanges({idField: 'id'}).
    pipe(
     // tap(x => console.log('stories service', x))
    );
  }

  getStory(idStory: string): Observable<Story> { // fetch une story avec sa reponse
    return this.db.collection('stories').doc(idStory).valueChanges().
    pipe(
      // tap(x => console.log('story service', x))
    );
  }

  getStoryAndResponse(idStory: string): Observable<Story> {
    return combineLatest(this.getStory(idStory), this.responseService.getResponse(idStory))
    .pipe(
      switchMap(([story, resp]) => of ({id: idStory, title: story.title, content: story.content, validated: story.validated,
                                        response: resp, hasResponse: typeof resp.id === 'undefined' ? false : true } as Story)),
      tap(x => console.log('story w response', x))
    );
  }

  /** POST: add a new story to the database */
  async addStory(story: Story): Promise<void> {
    try {
      const docRef = await this.db.collection('stories').add({
        title: story.title,
        content: story.content,
        hasResponse: false
      });
      console.log('Story written with ID: ', docRef.id);
      this.messagesService.sendMessageStory();
    } catch (error) {
      console.error('Error adding story: ', error);
    }
  }

   async updateValidated(story: Story): Promise<void> {
    try {
      this.db.collection('stories').doc(story.id).update({
        validated: story.validated,
      });
      console.log('Story updated with ID: ', story.id, 'validated :', story.validated);
    } catch (error) {
    console.error('Error update story: ', error);
    }
  }

  getUnrespondedStories(): Observable<Story[]>  {
    return this.stories$
      .pipe(
        map((stories: Story[]) => stories.filter(story => story.hasResponse === false)),
        tap(data => console.log('unresponded', data))
      );
  }

  getValidatedStories(): Observable<Story[]> {
    return this.stories$
      .pipe(
        map((stories: Story[]) => stories.filter(story => story.validated === true)),
        tap(x => console.log(x))
      );
  }

  getRespondedStories(): Observable<Story[]> {
    return this.stories$
      .pipe(
        map((stories: Story[]) => stories.filter(story => story.hasResponse === true),
        tap(x => console.log(x))));
  }
}
