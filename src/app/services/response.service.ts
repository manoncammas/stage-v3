import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { Response } from '../models/response';
import { catchError, tap, map, pluck, switchMap } from 'rxjs/operators';
import { AngularFirestore, QueryDocumentSnapshot } from '@angular/fire/firestore';
import { Story } from '../models/story';
import { StoryService } from './story.service';
import { MessagesService } from './messages.service';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  constructor(private db: AngularFirestore, private messagesService: MessagesService) { }

  /** GET response from the database */
  getResponses(): Observable<any> {
    return this.db.collectionGroup('responses').valueChanges().pipe(
      tap(x => console.log('responses service', x) )
    );
  }

  getResponse(idStory: string): Observable<Response> {
    return this.db.collection('stories').doc(idStory).collection('responses').valueChanges({idField: 'id'}).pipe(
      tap(x => console.log('response service', x) ),
      switchMap((response: any) => of({id: response[0]?.id, content: response[0]?.content} as Response)),
    );
  }

  async updateHasResponse(story: Story): Promise<void> {
    try {
      this.db.collection('stories').doc(story.id).update({
        hasResponse: true,
      });
      console.log('Story updated with ID: ', story.id, 'validated :', story.validated);
    } catch (error) {
    console.error('Error update story: ', error);
    }
  }

  async addResponse(story: Story, response: Response): Promise<void> {
    try {
      const docRef = await this.db.collection('stories').doc(story.id).collection('responses').add({
        content: response.content,
      });
      this.updateHasResponse(story);
      console.log('Response written with ID: ', docRef.id);
      this.messagesService.sendMessageResponse();
    } catch (error) {
      console.error('Error adding response: ', error);
    }
  }

}
