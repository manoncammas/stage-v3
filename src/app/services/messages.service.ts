import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor() { }

  sendMessageStory(): void {
    console.log('Message Service : NEW STORY POSTED');
  }

  sendMessageResponse(): void {
    console.log('Message Service : NEW RESPONSE POSTED');
  }
}
