import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Story } from '../models/story';
import { MessagesService } from '../services/messages.service';
import { StoryService } from '../services/story.service';

@Component({
  selector: 'app-add-story',
  templateUrl: './add-story.component.html',
  styleUrls: ['./add-story.component.css']
})
export class AddStoryComponent implements OnInit {

  stories: Story[] = [];

  submitted = false;
  show = false;
  autohide = true;

  storyForm = new FormGroup({
    title: new FormControl ('', Validators.required),
    content: new FormControl('', Validators.required),
    });

  constructor(private storyService: StoryService) { }

  ngOnInit(): void {
    this.getStories();
  }

  get f() { return this.storyForm.controls; }

  getStories(): void {
    this.storyService.getStories()
    .subscribe(stories  => this.stories = stories);
  }

  add(title: string, content: string): void {
    this.submitted = true;
    // stop here if form is invalid
    if (this.storyForm.invalid) {
        return;
    }
    title = title.trim();
    content = content.trim();
    if (!content && !title) { return; }
    this.storyService.addStory({ title, content } as Story);
    this.show = true;
    this.resetForm();
  }

  resetForm(): void {
    this.submitted = false;
    this.storyForm.reset();
  }
}
