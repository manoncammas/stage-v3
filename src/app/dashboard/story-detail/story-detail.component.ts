import { Component, Input, OnInit } from '@angular/core';
import { Story } from 'src/app/models/story';
import { Response } from 'src/app/models/response';
import { StoryService } from 'src/app/services/story.service';

@Component({
  selector: 'app-story-detail',
  templateUrl: './story-detail.component.html',
  styleUrls: ['./story-detail.component.css']
})
export class StoryDetailComponent implements OnInit {

  @Input() story?: Story;

  constructor(private storyService: StoryService) { }

  ngOnInit(): void {

  }

  save(): void {
    if (this.story) {
      this.storyService.updateValidated(this.story);
    }
    this.story = null;
  }
}
