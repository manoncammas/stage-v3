import { Component, OnDestroy, OnInit } from '@angular/core';

import { Story } from '../models/story';
import { StoryService } from '../services/story.service';
import { Response } from '../models/response';
import { ResponseService } from '../services/response.service';
import { concat, merge, Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { AngularFirestore, DocumentReference, QueryDocumentSnapshot } from '@angular/fire/firestore';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  stories$: Observable<Story[]>;
  responses: any[];

  selectedStory: Story; // histoire selectionnee
  selectedResponse?: Response; // reponse correspondant à l'histoire selectionee

  profileForm = new FormGroup({
    validated: new FormControl (false),
    answered: new FormControl(false),
    all: new FormControl(false),
    });

  constructor(private storyService: StoryService, private responseService: ResponseService, private db: AngularFirestore) { }

  ngOnInit(): void {
    this.stories$ = this.storyService.getStories();
  }

  onSubmit(): void {
    // TODO: Use EventEmitter with form value
    console.log(this.profileForm.value);
  }

  onSelect(story: any): void { // enregistre l'histoire et la reponse selectionnees
    this.storyService.getStoryAndResponse(story.id).subscribe(s => this.selectedStory = s);
  }

  filterDashboard(val: boolean, res: boolean, all: boolean): void {
    if (all === false)
    {
      if (val === true)
      {
        if (res === true) {
          this.stories$ = merge(this.storyService.getRespondedStories(), this.storyService.getValidatedStories());
        }
        else if (res === false) {
          this.stories$ = this.storyService.getValidatedStories();
        }
      }
      else if (val === false) {
        if (res === true) {
          this.stories$ = this.storyService.getRespondedStories();
        }
        else if (res === false) {
          this.stories$ = this.storyService.getStories();
        }
      }
    }
    else {
      this.stories$ = this.storyService.getStories();
    }
  }

  onChange(val: boolean, res: boolean, all: boolean): void {
    this.filterDashboard(val, res, all);
    console.log('validated: ', val);
    console.log('answered: ', res);
    console.log('all: ', all);
  }

}
