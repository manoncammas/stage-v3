import { Response } from './response';

export interface Story {
  id?: string;
  title?: string;
  content?: string;
  creationDate?: string;
  validated?: boolean;
  hasResponse?: boolean;
  response?: Response;
}
