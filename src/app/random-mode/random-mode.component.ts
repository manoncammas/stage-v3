import { Component, OnDestroy, OnInit } from '@angular/core';

import { Story } from '../models/story';
import { Response } from '../models/response';
import { StoryService } from '../services/story.service';
import { ResponseService } from '../services/response.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from '../services/messages.service';

@Component({
  selector: 'app-random-mode',
  templateUrl: './random-mode.component.html',
  styleUrls: ['./random-mode.component.css']
})
export class RandomModeComponent implements OnInit {

  randomStory?: Story; // histoire selectionee aléatoirement
  unrespondedStories: Story[]; // tableau des histoires sans réponses

  submitted = false;
  show = false;
  autohide = true;

  responseForm = new FormGroup({
    content: new FormControl('', Validators.required),
    });

  constructor(private storyService: StoryService, private responseService: ResponseService) { }

  ngOnInit(): void {
    this.storyService.getUnrespondedStories().subscribe(s => this.unrespondedStories = s);
  }

  get f() { return this.responseForm.controls; }

  random(): void { // fonction pour prendre une histoire au hasard parmi celles sans réponse
    this.randomStory = this.unrespondedStories[Math.floor(Math.random() * this.unrespondedStories.length)];
  }

  submit(content: string): Promise<void> {
    this.submitted = true;
    // stop here if form is invalid
    if (this.responseForm.invalid) {
        return;
    }
    content = content.trim();
    if (!content) {return; }
    this.responseService.addResponse(this.randomStory, { content } as Response);
    this.show = true;
    this.resetForm();
  }

  resetForm(): void {
    this.submitted = false;
    this.responseForm.reset();
    this.randomStory = null;
  }
}
